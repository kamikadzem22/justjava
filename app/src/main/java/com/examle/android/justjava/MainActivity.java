/**
 * IMPORTANT: Add your package below. Package name can be found in the project's AndroidManifest.xml file.
 * This is the package name our example uses:
 * <p>
 * package com.example.android.justjava;
 */

package com.examle.android.justjava;

import android.os.Bundle;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        display();
    }

    Coffee coffee = new Coffee.Builder()
            .priceForCup(2)
            .whippedCreamModifier(1)
            .chocolateModifier(1)
            .customerName()
            .whippedCream()
            .chocolate()
            .build();


    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        setCustomerName();
        orderAlert();
    }

    private void setCustomerName() {
        EditText editText = (EditText) findViewById(R.id.customer_name);
        if (editText.length() != 0)
            coffee.setCustomerName(editText.getText().toString());
        else
            coffee.setCustomerName(getResources().getString(R.string.defaultCustomerName));
    }

    public void resetAll(View view) {
        coffee.resetAll();
        coffee.resetCustomerName();
        resetCheckBoxes();
        display();
    }

    private void resetCheckBoxes() {
        CheckBox whippedCreamCheckBox = (CheckBox) findViewById(R.id.whipped_cream_checkbox);
        CheckBox chocolateCheckBox = (CheckBox) findViewById(R.id.chocolate_cream_checkbox);
        whippedCreamCheckBox.setChecked(false);
        chocolateCheckBox.setChecked(false);
    }

    public void increase(View view) {
        coffee.increaseQuantityOfCoffee();
        display();
    }

    public void decrease(View view) {
        if (coffee.isZero())
            return;
        coffee.decreaseQuantityOfCoffee();
        display();
    }

    public void onWhippedCreamClicked(View view) {
        CheckBox checkBox = (CheckBox) findViewById(R.id.whipped_cream_checkbox);
        coffee.setWhippedCream(checkBox.isChecked());
        displayPrice();
    }

    public void onChocolateClicked(View view) {
        CheckBox checkBox = (CheckBox) findViewById(R.id.chocolate_cream_checkbox);
        coffee.setChocolate(checkBox.isChecked());
        displayPrice();
    }

    /**
     * This method displays the given quantityOfCoffees value on the screen.
     */

    private void display() {
        displayPrice();
        displayQuantity();
    }

    private void displayQuantity() {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_TextView);
        quantityTextView.setText(String.valueOf(coffee.getQuantityOfCoffees()));
    }

    /**
     * This method displays the given priceForCup on the screen.
     */
    private void displayPrice() {
        TextView priceTextView = (TextView) findViewById(R.id.price_TextView);
        priceTextView.setText(getResources().getString
                (
                        R.string.total,
                        NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(coffee.calculatePrice())
                )
        );
    }

    private void orderAlert() {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
        dlgAlert.setMessage(
                getResources().getString
                        (
                                R.string.alertPay_Text,
                                coffee.getCustomerName(),
                                getResources().getQuantityString(R.plurals.cupsPlural, coffee.getQuantityOfCoffees(), coffee.getQuantityOfCoffees()),
                                genToppingsList(),
                                NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(coffee.calculatePrice())
                        )
        );
        dlgAlert.setPositiveButton(getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //dismiss the dialog
                    }
                });
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    private String genToppingsList() {
        String toppings = "";
        if (coffee.isWhippedCream())
            toppings += "\n" + getResources().getString(R.string.topping_whipped_cream);
        if (coffee.isChocolate())
            toppings += "\n" + getResources().getString(R.string.topping_chocolate);
        if (toppings.length() == 0)
            toppings += "\n" + getResources().getString(R.string.none);
        return toppings + "\n";
    }
}