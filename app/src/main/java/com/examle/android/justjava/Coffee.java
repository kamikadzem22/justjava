package com.examle.android.justjava;

/**
 * Created by kami on 8/23/17.
 */

public class Coffee {
    private int
            quantityOfCoffees,
            priceForCup,
            whippedCreamModifier,
            chocolateModifier;
    private boolean
            whippedCream,
            chocolate;
    private String customerName;

    public int calculatePrice() {
        return quantityOfCoffees * priceForCup +
                ((whippedCream) ? quantityOfCoffees : 0) * whippedCreamModifier +
                ((chocolate) ? quantityOfCoffees : 0) * chocolateModifier;
    }

    /*
    * Get
    */

    public int getQuantityOfCoffees() {
        return this.quantityOfCoffees;
    }

    public boolean isZero() {
        return (quantityOfCoffees == 0);
    }

    public int getPriceForCup() {
        return this.priceForCup;
    }


    public int getWhippedCreamModifier() {
        return this.whippedCreamModifier;
    }

    public boolean isWhippedCream() {
        return this.whippedCream;
    }

    public boolean isChocolate() {
        return this.chocolate;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    /*
    * Set
    */

    public void setWhippedCream(boolean whippedCream) {
        this.whippedCream = whippedCream;
    }

    public void setChocolate(boolean chocolate) {
        this.chocolate = chocolate;
    }

    public void setQuantityOfCoffees(int quantityOfCoffees) {
        this.quantityOfCoffees = quantityOfCoffees;
    }

    public void setPriceForCup(int priceForCup) {
        this.priceForCup = priceForCup;
    }

    public void setWhippedCreamModifier(int whippedCreamModifier) {
        this.whippedCreamModifier = whippedCreamModifier;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /*
    * Modify
    */

    public void increaseQuantityOfCoffee() {
        this.quantityOfCoffees++;
    }

    public void decreaseQuantityOfCoffee() {
        this.quantityOfCoffees--;
    }

    /*
    * Reset
    */

    public void resetQuantityOfCoffees() {
        this.quantityOfCoffees = 0;
    }

    public void resetWhippedCream() {
        this.whippedCream = false;
    }

    public void resetChocolate() {
        this.chocolate = false;
    }

    public void resetAll() {
        this.resetQuantityOfCoffees();
        this.resetWhippedCream();
        this.resetChocolate();
    }

    public void resetCustomerName() {
        setCustomerName("");
    }

    public static class Builder {
        private int
                quantityOfCoffees,
                priceForCup,
                whippedCreamModifier,
                chocolateModifier;
        private boolean
                whippedCream,
                chocolate;
        private String customerName;

        public Builder quantityOfCoffees() {
            this.quantityOfCoffees = 0;
            return this;
        }

        public Builder priceForCup(int value) {
            this.priceForCup = value;
            return this;
        }

        public Builder whippedCreamModifier(int value) {
            this.whippedCreamModifier = value;
            return this;
        }

        public Builder chocolateModifier(int value) {
            this.chocolateModifier = value;
            return this;
        }

        public Builder whippedCream() {
            this.whippedCream = false;
            return this;
        }

        public Builder chocolate() {
            this.chocolate = false;
            return this;
        }

        public Builder customerName() {
            this.customerName = "";
            return this;
        }

        public Coffee build() {
            return new Coffee(this);
        }
    }

    private Coffee(Builder builder) {
        this.quantityOfCoffees = builder.quantityOfCoffees;
        this.priceForCup = builder.priceForCup;
        this.whippedCreamModifier = builder.whippedCreamModifier;
        this.chocolateModifier = builder.chocolateModifier;
        this.whippedCream = builder.whippedCream;
        this.chocolate = builder.chocolate;
        this.customerName = builder.customerName;
    }
}
